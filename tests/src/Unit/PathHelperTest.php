<?php

declare(strict_types=1);

namespace Drupal\Tests\foxy\Unit;

use Composer\InstalledVersions;
use Drupal\Core\File\FileSystemInterface;
use Drupal\foxy\PathHelper;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\foxy\PathHelper
 * @group foxy
 */
class PathHelperTest extends UnitTestCase {

  /**
   * @covers ::getWebRoot
   *
   * @testWith [null, ""]
   *   ["/web", "web"]
   */
  public function testWebRoot(?string $project_root_suffix, string $expected_web_root): void {
    $project = InstalledVersions::getRootPackage();

    $file_system = $this->createMock(FileSystemInterface::class);
    // Ensure that a predictable project root is always returned from
    // FileSystemInterface::realpath().
    $file_system->expects($this->once())
      ->method('realpath')
      ->with($project['install_path'])
      ->willReturn('/path/to/project');

    $path_helper = new PathHelper($file_system, '/path/to/project' . $project_root_suffix);
    $this->assertSame($expected_web_root, $path_helper->getWebRoot());
  }

}
