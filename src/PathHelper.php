<?php

declare(strict_types=1);

namespace Drupal\foxy;

use Composer\InstalledVersions;
use Drupal\Core\File\FileSystemInterface;

/**
 * Computes paths needed for Foxy to work.
 */
final class PathHelper {

  public function __construct(
    private readonly FileSystemInterface $fileSystem,
    private readonly string $drupalRoot,
  ) {}

  /**
   * Returns the web root, relative to the project root.
   *
   * @return string
   *   The web root. For example, if Drupal is in `/my/project`, and the web
   *   root is `/my/project/web`, this will return `web`. If the web root and
   *   the project root are the same, this will return an empty string.
   */
  public function getWebRoot(): string {
    // Ask Composer where the project root is. This assumes we're in a site that
    // has been properly set up with Composer, which should be the case in all
    // modern Drupal sites.
    $project = InstalledVersions::getRootPackage();
    // Composer will return a relative path, so translate that to a fully
    // resolved absolute path we can compare with $this->drupalRoot.
    $project_root = $this->fileSystem->realpath($project['install_path']);
    assert(is_string($project_root));

    $web_root = str_replace($project_root, '', $this->drupalRoot);
    return trim($web_root, '/');
  }

}
