<?php

declare(strict_types=1);

namespace Drupal\foxy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Foxy form.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'foxy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['foxy.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['dist_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Compiled assets path'),
      '#required' => TRUE,
      '#default_value' => $this->config('foxy.settings')->get('dist_path'),
      '#description' => $this->t('Enter the path where your frontend bundler is configured to place compiled assets, relative to your web root.
      <br/>E.g. if working with vite, this should be the folder which contains your ".vite" and "assets" folders.
      <br/>If your vite manifest is at "/var/www/html/web/libraries/compiled/.vite/manifest.json" and "web" is your web root, enter "/libraries/compiled/" here.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save settings'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $dist_path = trim($form_state->getValue('dist_path'), '/ .');
    if (empty($dist_path)) {
      $form_state->setErrorByName('dist_path', $this->t('The path to your compiled assets must be a public directory inside your web root.'));
    }
    if (!is_dir($dist_path)) {
      $form_state->setErrorByName('dist_path', $this->t('Path to compiled assets does not exist or is not a directory.'));
    }
    $dist_path = '/' . $dist_path . '/';
    $form_state->setValue('dist_path', $dist_path);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('foxy.settings');
    $config->set('dist_path', $form_state->getValue('dist_path'));
    $config->save();
  }

}
